import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.css';
import React, { useEffect, useState } from 'react';
import { Link } from "react-router-dom";
import './index.css';

function App() {
  const [specs,setSpecs] = useState();

  const getSpecs = () =>{
    axios.get('http://127.0.0.1:8000/api/store-spaces')
    .then((response) => {
      setSpecs(response.data.data);
    })
    .catch(err=>{
      console.log(err);
    });
  }

  useEffect(()=>{
    getSpecs();
  },[])

  console.log(specs);

  return (
    <div className="App">
      <div className='container'>
        <div className='row'>
          <div className='col-lg-8 offset-2'>
            <div className='spec_head'>
              <h2>Explore our storage space, co-working space co-warehouse & services</h2>
              <p>Carefully crafted for a hassle-free and affordable experience</p>
            </div>
          </div>
        </div>
        <div className="row wrapper-area">
          {specs && specs.length > 0 && specs.map((item,index)=>(
            <div className="col-lg-3">
              <div className='spec-wrapper'>
                <div className="spec-img">
                    <img src={`http://127.0.0.1:8000/`+item.image} alt="" />
                </div>
                <div className="spec-details">
                    <h5>{item.title}</h5>
                    <div className="meta">
                        <p> {item.description.slice(0,68)}</p>
                        
                        <Link className='btn-bottom' to="#">Learn More <FontAwesomeIcon icon={faArrowRight} /></Link>
                    </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

export default App;
